import bodyParser from "body-parser"
import route from "./routes/route.js"
import  express  from "express"

const server = express()
const Port = process.env.Port || 9000

/**
 * 
 */
class app {

    // init module
    init(){
        server.use(express.json())
        server.use(bodyParser.urlencoded({extended: true }))
    }
    // init route
    route(){
        /**
         * Init all routes
         */
        route(server)
    }
    // start server and load all module /
    listen (){
        this.init()
        this.route()
        server.listen(Port, ()=>{
            console.log(`server running on port ${Port}`)
        })
    }
} 

export default new app 