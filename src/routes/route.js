
import {Router} from 'express'
import controller from '../module/controller/redis.controller.js'
import rate from '../middleware/rateLimit/rate.js'

const Route = Router()
const route = (app) =>{
    Route.get('/start',rate,new controller().index)
    app.use(Route)
}

export default route