import { RateLimiterRedis } from "rate-limiter-flexible";
import RedisClient from "../../module/redis/redis.js";

const opts = {
  storeClient: RedisClient,
  points: 5, // Number of points // Number of request per user.
  duration: 5, // Per second(s) // time for awaiting after limit rate execeded.

  execEvenly: false, // Do not delay actions evenly
  blockDuration: 0, // Do not block if consumed more than points
  keyPrefix: "rlflx", // must be unique for limiters with different purpose
};

const RateLimit = new RateLimiterRedis(opts);

export default async (request, response, next) => {
  // check ip provider.....
  let ip = request.headers["CF-Connecting-IP"] || request.ip;
  await RateLimit.consume(ip)
    .then((rateLimitResponse) => {
      next();
    })
    .catch((rejRes) => {
      if (rejRes instanceof Error) {
        response.status(500);
        throw Error("someting error ");
      } else {
        const secs = Math.round(rejRes.msBeforeNext / 1000) || 1;
        response
          .status(429)
          .send(`Too Many Requests retry after ${String(secs)}`);
      }
    });
};
