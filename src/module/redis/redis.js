import redis from "redis";
const RedisClient = redis.createClient({ legacyMode: true });
await RedisClient.connect()
RedisClient.on('error', (error)=>{
    if(error) throw new Error("redis server error ")
})

export default RedisClient